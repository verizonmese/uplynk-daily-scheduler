__author__ = 'v688763'

import sys
import os.path
import json
import urllib2, urllib, zlib, hmac, hashlib, time, json
#from datetime import datetime, timedelta
import datetime
import argparse
from Bag import Bag

ROOT_URL = 'http://services.uplynk.com'

full_day_starts = ['00:00:00']

#see if the file exists and open it
if os.path.exists('keys.json'):
    api_keys = open('keys.json','r')
    api = json.load(api_keys)
    OWNER = str(api['userid'])
    SECRET = str(api['apikey'])
    api_keys.close()

else:
    #if it doesn't exist we have error and print usage
    print('API keys are missing')
    print('Please use create_schedule_JSON.py to create them')
    sys.exit()

def Call(uri, **msg):
    msg['_owner'] = OWNER
    msg['_timestamp'] = int(time.time())
    msg = json.dumps(msg)
    msg = zlib.compress(msg, 9).encode('base64').strip()
    sig = hmac.new(SECRET, msg, hashlib.sha256).hexdigest()
    body = urllib.urlencode(dict(msg=msg, sig=sig))
    return json.loads(urllib2.urlopen(ROOT_URL + uri, body).read())

def CreateSchedule(args):
    if os.path.exists(args.inputfile):
        data_file = open(args.inputfile,'r')
        data = json.load(data_file)
        #gives me access to data['assets'] (dict)
        #and data['duration']
        data_file.close()
    else:
        print("File '" + args.inputfile + "' does not exist.")
        sys.exit()

    duration = int(data['duration'])
    #create full_day_starts list
    #this should be 86,400 seconds divided by the duration
    sked_blocks = int(86400 / duration) # this is the number of times the schedule should loop
    for x in xrange(duration, 86400, duration):
        loop_time = str(datetime.timedelta(seconds = int(x)))
        full_day_starts.append(loop_time)

    chan_ext_id = args.extid

    if (args.startdate):
        start_date = args.startdate
    else:
        #no date specified means, use tomorrow.  this is handy for daily CRON
        start_date = str(datetime.date.today() + datetime.timedelta(days=1))

    start_times = []
    for t in full_day_starts:
        start_times.append('%s %s' % (start_date,t))

    for st in start_times:
        starting_time = datetime.datetime.strptime(st, '%Y-%m-%d %H:%M:%S')
        sched = []

        for l in data['assets']:
            si = Bag()
            for k,v in l.items():
                if k == 'start':
                    dt = v.strip()
                    d,t = v.split()
                    h,m,s = t.split(':')  # got rid of frames
                    time_delta = datetime.timedelta(seconds = int(s),minutes = int(m),hours = int(h))
                    start = str(starting_time + time_delta)
                    setattr(si, 'start', start)
                else:
                    setattr(si, k, v)

            sched.append(si)
            if si.type == 'asset':
                print si.start + " " + si.external_id
            else:
                print si.start + " " + "ad"

        resp = Call('/api2/channel/schedule/update', external_id=chan_ext_id, schedule=sched)

        if int(resp['error']) > 0:
            print '>>> error loading %s: %s' % (st, resp['msg'])
        else:
            #print '>>> success: %s' % st
            print '--repeat--'

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--inputfile') #json filename
parser.add_argument('-d', '--startdate') #date to schedule channel
parser.add_argument('-e', '--extid') # external_id of channel

args = parser.parse_args()

if args.inputfile and args.extid:
    CreateSchedule(args)
else:
     print('missing required argument(s)')
     print 'USAGE:\n python schedule_channel.py \n   -i filename.json \n   -d yyyy-mm-dd \n   -e ext_id_of_channel'
