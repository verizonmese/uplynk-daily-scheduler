**[1] generate json schedule file**

```
$ python create_schedule_JSON.py  
   -a extid extid extid... (list assets you want scheduled in order)
   -o some_file.json 
   [-l length_in_seconds_of_ad_block] //leave out if no ads
   [-f frequence_in_seconds of ads] //default = 120, if not specified
   [-k uplynk_userID]
   [-s uplynk_APIKey]
```


NOTE: if this is the first time running you will need to include -k and -s

**[2] read the json into sched.py**

```
$ python schedule_channel.py 
   -i some_file.json  (the file you created above)
   -e ext_id_of_channel 
   [-d yyyy-mm-dd]
```
NOTE: if -d isn't specified the schedule will be set for tomorrow.  This is handy for step 3


*OPTIONAL:*
**[3] create a cron job to run step 2 once per day**

This will schedules a full day. The block you create will loop throughout the whole day.  Be sure to plan accordingly to fill the day in a clean multiple.  For example 4 hour block, 6 hour block, 8 hour block, etc.   Any number that divides nicely into 24 hours.