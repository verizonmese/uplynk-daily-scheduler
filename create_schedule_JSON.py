__author__ = 'v688763'

import sys
import os.path
import urllib2
import urllib
import zlib
import hmac
import hashlib
import json
import time
from xml.etree import ElementTree as ET
import argparse
import glob
import datetime

API_ROOT_URL = 'http://services.uplynk.com/api2/'

def Call(uri, **msg):
    msg['_owner'] = OWNER
    msg['_timestamp'] = int(time.time())
    msg = json.dumps(msg)
    msg = zlib.compress(msg, 9).encode('base64').strip()
    sig = hmac.new(SECRET, msg, hashlib.sha256).hexdigest()
    body = urllib.urlencode(dict(msg=msg, sig=sig))
    resp = json.loads(urllib2.urlopen(API_ROOT_URL + uri, body).read())
    return resp

def get_utc_formatted_datetime(local_datetime):
    local_formatted_date_time = datetime.datetime.strptime(local_datetime, '%Y-%m-%d %H:%M:%S')

    ltime=time.localtime()
    is_dst=ltime.tm_isdst ==1
    if(is_dst):
        toffset=time.altzone
    else:
        toffset=time.timezone

    utc_schedule_date_time = local_formatted_date_time + datetime.timedelta(seconds=toffset)
    utc_formatted_date_time = utc_schedule_date_time.strftime("%Y-%m-%d %H:%M:%S")

    return(utc_formatted_date_time)

def GenerateScheduleFile(assets, outfile):
    start_time = datetime.datetime.strptime(get_utc_formatted_datetime('1980-01-01 00:00:00'), '%Y-%m-%d %H:%M:%S')
    list_assets = Call('asset/get', external_ids=assets)['assets']
    sked=[]
    dur=0
    totaldur=0
    if (args.adfreq):
        adfreq = int(args.adfreq)
    else:
        adfreq = 240

    for asset in list_assets:
        asset_dur = round(asset['duration'])
        asset_start = (start_time + datetime.timedelta(seconds=totaldur)).strftime("%Y-%m-%d %H:%M:%S")
        sked.append(dict(type='asset', external_id=asset['external_id'], start=(asset_start)))
        totaldur += asset_dur # total duration
        dur += asset_dur #since last ad

        if (args.length): # only insert ads if this is specified
            if(dur>adfreq): #At what duration you want an ad, it will not cut off content it will come after the end of piece playing during that time
                ad_start = (start_time + datetime.timedelta(seconds=totaldur)).strftime("%Y-%m-%d %H:%M:%S")
                sked.append(dict(type='ad', start = ad_start))
                totaldur += int(args.length) #adjust this time to increase or descrease ad breaks legnth
                dur=0
 
    json_data = {}
    json_data['duration'] = totaldur
    json_data['assets'] = sked
    sked_file = open(outfile, 'w')
    sked_file.write(json.dumps(json_data,indent=4, separators=(',', ': ')))
    sked_file.close()
    print ('*** Saved schedule file to {0} ***'.format(outfile))

def GetAssetInfo(ext_id):
    return Call('asset/get', external_id=ext_id)

def GetAssetList():
    assets =  Call('asset/list')['assets']
    ar = []
    for asset in assets:
        ar.append(asset['external_id'])

parser = argparse.ArgumentParser()
parser.add_argument('-a', '--assets', nargs='+')
parser.add_argument('-l', '--length') # ad length in seconds (optional. if not included no ads will be inserted)
parser.add_argument('-o', '--outfile') # json file to output
parser.add_argument('-k', '--apikey')
parser.add_argument('-s', '--apisecret')
parser.add_argument('-f', '--adfreq')

args = parser.parse_args()

#if keys are included make a keys.json file
if (args.apikey and args.apisecret):
    OWNER = args.apikey
    SECRET = args.apisecret
    key_file = open('keys.json', 'w')
    key_file.write(json.dumps({'userid': OWNER, 'apikey': SECRET},indent=4, separators=(',', ': ')))
    key_file.close()

else:
    #see if the file exists and open it
    if os.path.exists('keys.json'):
        api_keys = open('keys.json','r')
        api = json.load(api_keys)
        OWNER = str(api['userid'])
        SECRET = str(api['apikey'])
        api_keys.close()

    else:
        #if it doesn't exist we have error and print usage
        print('missing required argument(s)')
        print 'USAGE: \n python create_schedule.py \n\
           -k UserID \n\
           -s APIKey \n\
           -a external_id1 [external_id2...] \n\
           -o outputfile.json \n\
           [-l length_of_ads_in_seconds] \n\
           [-f frequency_of_ads_in_seconds]'
        sys.exit()

if (args.assets and args.outfile):
    GenerateScheduleFile(args.assets, args.outfile)
else:
    print('missing required argument(s)')
    print 'USAGE:\n python create_schedule.py \n\
       -a external_id1 [external_id2...] \n\
       -o outputfile.json \n\
       [-l length_of_ads_in_seconds] \n\
       [-f frequency_of_ads_in_seconds]'
